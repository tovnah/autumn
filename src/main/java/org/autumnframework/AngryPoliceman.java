package org.autumnframework;

import javax.annotation.PostConstruct;

public class AngryPoliceman implements Policeman {
    @InjectByType
    private Recommendator recommendator;

    @PostConstruct
    public void init() {
        System.out.println(recommendator.getClass());
    }

    public void makePeopleLeaveRoom() {
        System.out.println("killall -9");
        recommendator.recommend();
    }
}
