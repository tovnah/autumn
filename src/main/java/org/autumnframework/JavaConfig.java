package org.autumnframework;

import lombok.Getter;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

public class JavaConfig implements Config {
    @Getter
    private Reflections scanner;
    private Map<Class, Class> ifc2ImplClass;

    public JavaConfig(String packageToScan, Map<Class, Class> ifc2ImplClass) {
        this.ifc2ImplClass = ifc2ImplClass;
        this.scanner = new Reflections(packageToScan);
    }

    public <T> Class<? extends T> getImplClass(Class<T> intrface) {
        return ifc2ImplClass.computeIfAbsent(intrface, aClass -> {
            Set<Class<? extends T>> classes = scanner.getSubTypesOf(intrface);
            if (classes.size() != 1) {
                throw new RuntimeException(intrface + " has 0 or mare than one impl, please update your config");
            }
            return classes.iterator().next();
        });
    }
}
