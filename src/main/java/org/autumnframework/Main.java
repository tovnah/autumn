package org.autumnframework;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Class, Class> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(Policeman.class, AngryPoliceman.class);

        ApplicationContext context = Application.run("org.autumnframework", objectObjectHashMap);
        CoronaDesinfector coronaDesinfector = context.getObject(CoronaDesinfector.class);
        coronaDesinfector.start(new Room());
    }
}
