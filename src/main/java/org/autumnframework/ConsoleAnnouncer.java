package org.autumnframework;

public class ConsoleAnnouncer implements Announcer {
    @InjectByType
    private Recommendator recommendator;

    public void announce(String s) {
        System.out.println(s);
        recommendator.recommend();
    }
}
