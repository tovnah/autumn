package org.autumnframework;

import org.reflections.Reflections;

public interface Config {
    <T> Class<? extends T> getImplClass(Class<T> intrface);

    Reflections getScanner();
}
