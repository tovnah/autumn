package org.autumnframework;

import net.sf.cglib.proxy.Enhancer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DeprecatedHandlerProxyConfiguratorImpl implements ProxyConfigurator {

    //Выенсти в отдельный класс
    @Override
    public Object replaceWithProxyIfNeeded(Object original, Class type) {
        if (type.isAnnotationPresent(Deprecated.class)) {
            if (type.getInterfaces().length == 0) {
                return Enhancer.create(type, (net.sf.cglib.proxy.InvocationHandler) (proxy, method, args) -> getInvocationHandlerLogic(original, method, args));
            } else {
                return replaceWithProxy(original, type);
            }
        }

        return original;
    }

    private Object replaceWithProxy(Object original, Class type) {
        return Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), (proxy, method, args) -> getInvocationHandlerLogic(original, method, args));
    }

    private Object getInvocationHandlerLogic(Object original, Method method, Object[] args) throws IllegalAccessException, InvocationTargetException {
        System.out.println("********************* что ж ты делаешь урод!!");

        return method.invoke(original, args);
    }
}
