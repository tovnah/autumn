package org.autumnframework;

public interface Announcer {
    void announce(String s);
}
