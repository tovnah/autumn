package org.autumnframework;

public interface ProxyConfigurator {
    Object replaceWithProxyIfNeeded(Object original, Class type);
}
