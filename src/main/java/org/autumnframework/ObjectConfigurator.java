package org.autumnframework;

import lombok.SneakyThrows;

public interface ObjectConfigurator {
    void configure(Object t, ApplicationContext context);
}
