package org.autumnframework;

@Singleton
//@Deprecated
public class RecommendatorImpl implements Recommendator {

    @InjectProperty
    private String alcohol;

    public RecommendatorImpl() {
        System.out.println(this.getClass().getSimpleName() + " was created");
    }

    @Override
    public void recommend() {
        System.out.println("drink " + alcohol);
    }
}
