package org.autumnframework;

public interface Recommendator {
    void recommend();
}
